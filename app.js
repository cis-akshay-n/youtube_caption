var express             = require("express"),
    mongoose            = require("mongoose"),
    app                 = express(),
    bodyParser          = require("body-parser"),
    fs                  = require("fs"),
    Video               = require("./models/video"),
    YouTube             = require('youtube-node'),
    getYoutubeSubtitles = require('@joegesualdo/get-youtube-subtitles-node');

 
var youTube = new YouTube();
var googleApikey = 'AIzaSyBYOIldhjxIT5gkh67p3QCdHuYcZNpmm7c';
youTube.setKey(googleApikey);


// mongodb setup
mongoose.Promise = global.Promise;
 mongoose.connect("mongodb://localhost/youtubeSubtitle",function(err,db){
});


app.set("view engine", "ejs");
app.use(express.static(__dirname +"/views"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname +"/public"));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Accept');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get("/", function(req, res){
    res.render("index",{status:100,text:"",data:null,search:""});
});

/* video details by id*/ 
app.post("/getSubtitle",function(req,res){
    var url_s = req.body.url;
    var ID = '';
    url = url_s.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if(url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_\-]/i);
        ID = ID[0];
    }
    else {
        ID = null;
    }

    if(ID != null){
        youTube.getById(ID, function(error, result) {
          if (error || result.items.length == 0) {
            res.render("index",{status:404,text:"The youtube url is not valid",data:null,search:url_s});
          }
          else {
            var title = result.items[0].snippet.title;
            getYoutubeSubtitles(ID, {type: 'auto'})
            .then(subtitles => {
                var wordsArray = [];
                for(var i=0;i<subtitles.length;i++){
                    wordsArray = wordsArray.concat(subtitles[i].words);
                }
                Video.find({video_id:ID},function(err,doc){
                    if(!err && doc.length){
                        res.render("index",{status:200,text:"",data:{id:ID,title:title,words:wordsArray.length},search:url_s});
                    }else{
                        if(err){
                            res.render("index",{status:404,text:"The youtube url is not valid",data:null,search:url_s});
                        }else{
                            Video.create({video_id:ID,title:title,words:wordsArray},function(error,data){
                                if(!error){
                                     res.render("index",{status:200,text:"",data:{id:ID,title:title,words:wordsArray.length},search:url_s});
                                }else{
                                     res.render("index",{status:404,text:"Error in inserting data",data:null,search:url_s});
                                }
                            });
                        }
                    }
                });
                })
            .catch(err => {
              res.render("index",{status:404,text:"subtitle for the video is not available",data:null,search:url_s});
            })
          }
        });

    }else{
       console.log("The youtube url_s is not valid.");
       res.render("index",{status:404,text:"The youtube url is not valid",data:null,search:url_s});
    }
});
app.listen(process.env.PORT | 4000, process.env.IP, function(){
    console.log("Server has started in port 4000");
});