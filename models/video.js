var mongoose = require("mongoose");

var videoSchema = new mongoose.Schema({
    video_id:String,
    title: String,
    words: Array,
});

module.exports = mongoose.model("Video", videoSchema);